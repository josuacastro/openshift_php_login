<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Hash-Utilities</title>
		<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
	</head>

  <body>
		<div class="container">
			<h1>Hash-Utilities <small>by @mariofont</small></h1>
			<p class="lead">Tools for BCrypt's implementation in PHP. Code: <a href="https://github.com/mariofont/Hash-Utilities" target="_blank">GitHub</a>.</p>

			<hr>

			<h3>Text to hash</h3>
			<form action="index.php" method="post">
			  <div class="form-group">

			    <input type="text" name="text" class="form-control" id="examplePlainText" placeholder="Text">
			  </div>
			  <button name="submitOne" type="submit" class="btn btn-default">Hash</button>
			</form>

			<?php

				if(isset($_POST['submitOne'])) {

					$text = $_POST['text'];
					$hash = password_hash("$text", PASSWORD_DEFAULT)."\n";

					if (empty($_POST['text'])) {
						?>
						&nbsp;
						<div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Warning!</strong> Text field is empty.
						</div>
						<?php
					} else {
						?>
						&nbsp;
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Success!</strong> Hash code: <?php echo $hash ?>
						</div>
						<?php
					}

				}
				?>

				<hr>

				<h3>Hash verifier</h3>
				<form action="index.php" method="post">
					<div class="form-group">
						<label for="exampleHash">Hash</label>
						<input type="text" name="hash" class="form-control" id="exampleHash" placeholder="Hash">
						<label for="exampleWord">Text</label>
						<input type="text" name="word" class="form-control" id="exampleWord" placeholder="Text">
					</div>
					<button name="submitTwo" type="submit" class="btn btn-default">Verify</button>
				</form>


				<?php

					if(isset($_POST['submitTwo'])) {

						$hashTwo = $_POST['hash'];
						$textTwo = $_POST['word'];

						if ((empty($_POST['hash'])) || (empty($_POST['word']))) {
							?>
							&nbsp;
							<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Warning!</strong> One of the fields is empty.
							</div>
							<?php
						} else if (password_verify($textTwo, $hashTwo)) {
							?>
							&nbsp;
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Success!</strong> They match.
							</div>
							<?php
						} else {
							?>
							&nbsp;
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Error!</strong> The text doesn't match the hash.
							</div>
							<?php
						}
					}
					?>

		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
